Description
===========
Ubercart Everyday integrates Everyday online payment method 
(Everyday-verkkomaksu) with Ubercart. 

Everyday is a Finnish billing and part payment online gateway provider.
See more at http://www.everyday.fi/verkkomaksu/

Istallation instruction for Ubercart Everyday 
=============================================
1. Install Ubercart Everyday module as usual

2. Enable Ubercart Everyday payment method 
   (admin/store/settings/payment)

3. By default module is in test mode (with test mode credentials provided)

4. Configure Everyday settings 
   (admin/store/settings/payment/method/everyday). 
   You'll need to create Everyday-verkkomaksu storekeeper account and 
   obtain a customer ID and secrect key via 
   https://kauppias.opr-vakuus.fi/sopimuspohja.php. 
